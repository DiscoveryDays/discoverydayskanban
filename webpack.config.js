var webpack = require('webpack');

module.exports = {
    entry: './src/index.js',
    output: {
        path: __dirname,
        filename: 'bundle.js'
    },
    node: {
        fs: 'empty', // stub out fs, doesn't seem required for our dev...
        net: 'empty',
        tls: 'empty'
    },
    devtool: 'source-map'
};