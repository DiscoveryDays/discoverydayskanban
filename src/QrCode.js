const QRCode = require('qrcode');

const generateButton = document.getElementById("generate");
generateButton.onclick = () => {
    const canvas = document.getElementById('qrCanvas');

    const cardClient = document.getElementById("cardClient").value;
    const cardValue = document.getElementById("cardValue").value;
    const cardName = document.getElementById("cardName").value;
    const cardCategory = document.getElementById("cardCategory").value;
    const cardType = document.getElementById("cardType").value;
    const cardTeam = document.getElementById("cardTeam").value;
    const cardSize = document.getElementById("cardSize").value;

    const qrValue = [cardClient, cardValue, cardName, cardCategory, cardType, cardTeam, cardSize].join("|");

    QRCode.toCanvas(qrValue, function (error, canvas) {
        if (error) console.error(error)

        // const qrColumn = document.createElement('div')
        // qrColumn.setAttribute('class', 'col')
        // qrColumn.appendChild(canvas)

        // const clientColumn = document.createElement('div')
        // clientColumn.setAttribute('class', 'col')
        // clientColumn.innerHTML = cardClient

        // const clientRow = document.createElement('div')
        // clientRow.setAttribute('class', 'row')
        // clientRow.appendChild(clientColumn)
        
        // const valueColumn = document.createElement('div')
        // valueColumn.setAttribute('class', 'col')
        // valueColumn.innerHTML = cardValue

        // const valueRow = document.createElement('div')
        // valueRow.setAttribute('class', 'row')
        // valueRow.appendChild(valueColumn)
        
        // const secondColumn = document.createElement('div')
        // secondColumn.setAttribute('class', 'col')
        // secondColumn.appendChild(clientRow)
        // secondColumn.appendChile(valueRow)

        // const row = document.createElement('div')
        // row.setAttribute('class', 'row')
        // row.appendChild(qrColumn)
        // row.appendChild(secondColumn)

        // var container = document.getElementById('qrCodes')
        // container.appendChild(row)
        var container = document.getElementById('qrCodes')
        container.appendChild(canvas)
    })
};
