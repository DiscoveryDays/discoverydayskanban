import Instascan from 'instascan';
import request from 'request-promise-native';

const scannedItems = new Set();

const scansElement = document.getElementById('scans');
const previewElement = document.getElementById('preview');
const camerasElement = document.getElementById('cameras');

let isMirrored = true;
let currentCamera;
let scanner;

const deleteScannedItem = scannedItem => {
  scannedItems.delete(scannedItem);
  reloadScannedItemsOnScreen();
};

const addScannedItem = scannedItem => {
  scannedItems.add(scannedItem);
  reloadScannedItemsOnScreen();
}

const clearScannedItems = () => {
  scannedItems.clear();
  reloadScannedItemsOnScreen();
};

const reloadScannedItemsOnScreen = () => {
  scansElement.innerHTML = '';

  const createScannedItemElement = scannedItem => {
    const scannedItemColElement = document.createElement('td');
    scannedItemColElement.innerHTML = scannedItem;

    const deleteIcon = document.createElement('i');
    deleteIcon.className = 'fa fa-trash-o';
    deleteIcon.style.cursor = 'pointer';
    deleteIcon.onclick = () => deleteScannedItem(scannedItem);

    const deleteColElement = document.createElement('td');
    deleteColElement.style.width = '50px';
    deleteColElement.style.textAlignLast = 'center';
    deleteColElement.appendChild(deleteIcon);

    const rowElement = document.createElement('tr');
    rowElement.appendChild(deleteColElement);
    rowElement.appendChild(scannedItemColElement);

    return rowElement;
  }

  scannedItems.forEach(scannedItem => scansElement.appendChild(createScannedItemElement(scannedItem)));
};

const createScanner = () => {
  scanner = new Instascan.Scanner({
    video: previewElement,
    mirror: isMirrored,
    refractoryPeriod: 1000,
  });

  scanner.addListener('scan', scannedItem => {
    console.log(`scanned: ${scannedItem}`);

    addScannedItem(scannedItem);
  });
}

const startScanning = camera => {
  currentCamera = camera || currentCamera;
  scanner.start(currentCamera);
}

const addCameraToScreen = camera => {
  const cameraButton = document.createElement('div');
  cameraButton.className = "btn btn-outline-primary";
  cameraButton.style.display = 'block';
  cameraButton.style.marginBottom = '2px';
  cameraButton.innerHTML = camera.name;
  cameraButton.onclick = () => startScanning(camera);

  camerasElement.appendChild(cameraButton);
};

const createCameraSelection = cameras => {
  if (!cameras.length) {
    camerasElement.innerHTML = 'No cameras found';
    return;
  }

  cameras.forEach(addCameraToScreen);
}

Instascan.Camera.getCameras()
  .then(createCameraSelection)
  .catch(e => console.error(e));

document.getElementById('mirrorCam').onclick = () => {
  if (!currentCamera) return;

  isMirrored = !isMirrored;

  scanner.stop();

  createScanner();

  startScanning();
}

createScanner();

document.getElementById('digitalize').onclick = () => {
  const status = document.getElementById('statusColumn').value;
  const items = [...scannedItems];

  //for demo ?
  // alert(`updating status to ${status} for items:\n${items.join("\n")}`);
  // clearScannedItems();

  request({
    method: 'post',
    body: {
      column: status,
      workitems: [...scannedItems]
    },
    json: true,
    url: 'http://localhost:61344/api/Kanban'
  }).then(() => {
    alert(`Sent update to status ${status} for items:\n${items.join("\n")}`);
    clearScannedItems();
  })
    .catch(err => alert(`Failed to send new status: ${err}`));

};

export { };